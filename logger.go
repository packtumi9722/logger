package logger

import (
	"net"
	"os"
	"runtime"
	"sync"

	logrustash "github.com/bshuster-repo/logrus-logstash-hook"
	"github.com/sirupsen/logrus"
)

// logrus hooks to logstash
var logger *logrus.Logger
var once sync.Once

// Fields == logrus.Fields
type Fields = logrus.Fields

// self log level define
const (
	DebugLevel = logrus.DebugLevel
	FatalLevel = logrus.FatalLevel
	PanicLevel = logrus.PanicLevel
	ErrorLevel = logrus.ErrorLevel
	InfoLevel  = logrus.InfoLevel
	WarnLevel  = logrus.WarnLevel
	TraceLevel = logrus.TraceLevel
)

// Setup logger instance
func Setup(addr, servername string) {
	once.Do(func() {
		logger = logrus.New()
		var conn net.Conn
		// if log server exist, send log to log server
		if len(addr) != 0 {
			conn, _ = net.Dial("tcp", addr)
		}

		if conn != nil {
			hook := logrustash.New(conn, logrustash.DefaultFormatter(Fields{"server": servername}))
			logger.Hooks.Add(hook)
		} else {
			logger.SetFormatter(&logrus.JSONFormatter{})
			// logger.SetFormatter(&logrus.TextFormatter{
			// 	ForceColors:   true,
			// 	FullTimestamp: true,
			// })
			logger.SetOutput(os.Stdout)
			logger.SetLevel(logrus.TraceLevel)
		}
	})
}

// Push msg to log server
func Push(level logrus.Level, msg interface{}, comment ...interface{}) {
	funcName := "unknown"
	pc, _, _, ok := runtime.Caller(1)
	if ok {
		funcName = runtime.FuncForPC(pc).Name()
	}

	ctx := logger.WithField("method", funcName)
	if len(comment) > 0 {
		ctx = ctx.WithField("comment", comment[0])
	}

	switch level {
	case PanicLevel:
		ctx.Panic(msg)
	case FatalLevel:
		ctx.Fatal(msg)
	case ErrorLevel:
		ctx.Error(msg)
	case WarnLevel:
		ctx.Warn(msg)
	case InfoLevel:
		ctx.Info(msg)
	case TraceLevel:
		ctx.Trace(msg)
	case DebugLevel:
		fallthrough
	default:
		ctx.Debug(msg)
	}
}

// PanicLevel Level = iota
// 	// FatalLevel level. Logs and then calls `logger.Exit(1)`. It will exit even if the
// 	// logging level is set to Panic.
// 	FatalLevel
// 	// ErrorLevel level. Logs. Used for errors that should definitely be noted.
// 	// Commonly used for hooks to send errors to an error tracking service.
// 	ErrorLevel
// 	// WarnLevel level. Non-critical entries that deserve eyes.
// 	WarnLevel
// 	// InfoLevel level. General operational entries about what's going on inside the
// 	// application.
// 	InfoLevel
// 	// DebugLevel level. Usually only enabled when debugging. Very verbose logging.
// 	DebugLevel
// 	// TraceLevel level. Designates finer-grained informational events than the Debug.
// 	TraceLevel
