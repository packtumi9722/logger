module gitlab.com/packtumi9722/logger

go 1.15

require (
	github.com/bshuster-repo/logrus-logstash-hook v1.0.0
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
)
